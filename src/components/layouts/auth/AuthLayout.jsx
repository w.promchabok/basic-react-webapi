import DocumentTitle from "react-document-title";
const AuthLayout = ({ children, title = "Smart Stock" }) => {
  return (
    <>
      <DocumentTitle title={title + " | Smart Stock"} />
      <main className="d-flex w-100">
        <div className="container d-flex flex-column">
          <div className="row vh-100">
            <div className="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
              {children}
            </div>
          </div>
        </div>
      </main>

    </>
  );
};

export default AuthLayout;
