import DocumentTitle from "react-document-title";
import Footer from "../../shared/backend/Footer";
import Sidebar from "../../shared/backend/Sidebar";
import Navbar from "../../shared/backend/Navbar";

const BackendLayout = ({ children, title = "Dashboard" }) => {
  return (
    <>
      <DocumentTitle title={title + " | Smart Stock"} />
      <div className="wrapper">
        <Sidebar />
        <div className="main">
          <Navbar />
          <main className="content">
            {children}
          </main>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default BackendLayout;
