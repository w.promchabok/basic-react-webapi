import { NavLink } from "react-router-dom";

const Sidebar = () => {
  return (
    <>
      <nav id="sidebar" className="sidebar js-sidebar">
        <div className="sidebar-content js-simplebar">
          <NavLink className="sidebar-brand" to="index.html">
            <span className="align-middle">AdminKit</span>
          </NavLink>
          <ul className="sidebar-nav">
            <li className="sidebar-header">Pages</li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="/backend/dashboard">
                <i className="align-middle" data-feather="sliders" />{" "}
                <span className="align-middle">Dashboard</span>
              </NavLink>
            </li>           
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="pages-profile.html">
                <i className="align-middle" data-feather="user" />{" "}
                <span className="align-middle">Profile</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="pages-sign-in.html">
                <i className="align-middle" data-feather="log-in" />{" "}
                <span className="align-middle">Sign In</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="pages-sign-up.html">
                <i className="align-middle" data-feather="user-plus" />{" "}
                <span className="align-middle">Sign Up</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="pages-blank.html">
                <i className="align-middle" data-feather="book" />{" "}
                <span className="align-middle">Blank</span>
              </NavLink>
            </li>
            {/* stock */}
            <li className="sidebar-header">Stock</li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="/backend/product">
                <i className="align-middle" data-feather="sliders" />{" "}
                <span className="align-middle">Product</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="/backend/report">
                <i className="align-middle" data-feather="sliders" />{" "}
                <span className="align-middle">Report</span>
              </NavLink>
            </li>

            <li className="sidebar-header">Tools &amp; Components</li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="ui-buttons.html">
                <i className="align-middle" data-feather="square" />{" "}
                <span className="align-middle">Buttons</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="ui-forms.html">
                <i className="align-middle" data-feather="check-square" />{" "}
                <span className="align-middle">Forms</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="ui-cards.html">
                <i className="align-middle" data-feather="grid" />{" "}
                <span className="align-middle">Cards</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="ui-typography.html">
                <i className="align-middle" data-feather="align-left" />{" "}
                <span className="align-middle">Typography</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="icons-feather.html">
                <i className="align-middle" data-feather="coffee" />{" "}
                <span className="align-middle">Icons</span>
              </NavLink>
            </li>
            <li className="sidebar-header">Plugins &amp; Addons</li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="charts-chartjs.html">
                <i className="align-middle" data-feather="bar-chart-2" />{" "}
                <span className="align-middle">Charts</span>
              </NavLink>
            </li>
            <li className="sidebar-item">
              <NavLink className="sidebar-link" to="maps-google.html">
                <i className="align-middle" data-feather="map" />{" "}
                <span className="align-middle">Maps</span>
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
};

export default Sidebar;
