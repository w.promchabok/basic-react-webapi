import DocumentTitle from "react-document-title";
import { Link } from "react-router-dom";

const PageNotFound = () => {
  return (
    <>
      <link rel="stylesheet" href="assets/vendor/css/pages/page-misc.css" />
      <DocumentTitle title={"Page Not Found | Smart Stock"} />
      <main className="d-flex w-100">
        <div className="container d-flex flex-column">
          <div className="row vh-100">
            <div className="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
              <div className="d-table-cell align-middle">
                <div className="text-center mt-4">
                  <h1 className="h2">Page Not Found</h1>
                  <p className="lead">
                    Please check your page url or contact Admin.
                  </p>
                </div>
                <div className="card">
                  <div className="card-body">
                    <div className="m-sm-3">
                    <img src="../assets/img/photos/404-page-not-found.jpg" alt="404-page-not-found" width="450px"/>
                    </div>
                  </div>
                </div>
                <div className="text-center mb-3">
                  Go to  <Link to="/backend/dashboard">Dashboard</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default PageNotFound;
