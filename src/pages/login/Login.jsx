import { useState } from "react";
import AuthLayout from "../../components/layouts/auth/AuthLayout";
import { Link, useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'


const Login = () => {
  

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  let navigate = useNavigate();

  if (localStorage.getItem("fullname")) {
    navigate("/backend/dashboard");
  }
  
  function handleClick() {
    navigate("/backend/dashboard");
  }

  const handleLogin = (e) => {
    e.preventDefault();
    //console.log({ username, password });
    if(username === 'admin@demo.com' && password === '1234') {
      let timerInterval;
      Swal.fire({
        html: "กำลังเข้าสู่ระบบ <b></b>",
        timer: 2000,
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading();
          const timer = Swal.getPopup().querySelector("b");
          timerInterval = setInterval(() => {
          }, 100);
        },
        willClose: () => {
          clearInterval(timerInterval);
        }
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          handleClick();
          localStorage.setItem('fullname', 'Admin');
        }
      });
      
    }else{
      Swal.fire({
        icon: "error",
        title: "มีข้อผิดพลาด",
        text: " คุณไม่ใช่แอดมิน!",
        footer: '<Link to="#">Why do I have this issue?</Link>'
      });
    }

  }


  return (
    <AuthLayout title="Login">
    <div className="d-table-cell align-middle">
      <div className="text-center mt-4">
        <h1 className="h2">Welcome back!</h1>
        <p className="lead">
          Sign in to your account to continue
        </p>
      </div>
      <div className="card">
        <div className="card-body">
          <div className="m-sm-3">
            <form onSubmit={ handleLogin }>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input className="form-control form-control-lg" 
                type="email" 
                name="email" 
                placeholder="Enter your email"
                onChange={ e => setUsername(e.target.value) }
                value={username}
                />
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input className="form-control form-control-lg" 
                type="password" 
                name="password" 
                placeholder="Enter your password"
                onChange={ e => setPassword(e.target.value) }
                value={password}
                />
              </div>
              <div>
                <div className="form-check align-items-center">
                  <input id="customControlInline" type="checkbox" className="form-check-input" defaultValue="remember-me" name="remember-me" defaultChecked />
                  <label className="form-check-label text-small" htmlFor="customControlInline">Remember me</label>
                </div>
              </div>
              <div className="d-grid gap-2 mt-3">
              <input
                  type="submit"
                  className="btn btn-primary"
                  name="submit"
                  id="submit"
                  value="เข้าสู่ระบบ"
              />         
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="text-center mb-3">
        Don't have an account? <Link to="/register">Sign up</Link>
      </div>
    </div>
    </AuthLayout>
  );
};

export default Login;
