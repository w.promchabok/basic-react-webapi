import AuthLayout from "../../components/layouts/auth/AuthLayout";
import { Link } from "react-router-dom";
import { useForm } from 'react-hook-form'

const Register = () => {

  const { handleSubmit, register, formState: { errors } } = useForm();
  const onSubmit = values => console.log(values);


  return (
    <AuthLayout title="Register">
      <div className="d-table-cell align-middle">
        <div className="text-center mt-4">
          <h1 className="h2">Get started</h1>
          <p className="lead">
            Start creating the best possible user experience for you customers.
          </p>
        </div>
        <div className="card">
          <div className="card-body">
            <div className="m-sm-3">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                  <label className="form-label">Full name</label>
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    id="fullname"
                    name="fullname"
                    placeholder="Enter your fullname"
                    {...register("fullname", {
                      required: "Required",
                      validate: value => value !== "admin" || "Nice try!"
                    })}
                  />
                  {errors.fullname && errors.fullname.message}
         
                </div>
                <div className="mb-3">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control form-control-lg"
                    type="email"
                    name="email"
                    placeholder="Enter your email"
                    {...register("email", {
                      required: "Required",
                      pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                        message: "invalid email address"
                      }
                    })}
                  />
                  {errors.email && errors.email.message}
                </div>
                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input
                    className="form-control form-control-lg"
                    type="password"
                    name="password"
                    placeholder="Enter password"
                  />
                </div>
                <div className="d-grid gap-2 mt-3">
                  <Link to="/login" type="submit" className="btn btn-lg btn-primary">
                    Sign up
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="text-center mb-3">
          Already have account? <Link to="/login">Log In</Link>
        </div>
      </div>
    </AuthLayout>
  );
};

export default Register;
