import React from 'react'
import { Routes, Route } from 'react-router-dom'

import Login from './pages/login/Login'
import Register from './pages/register/Register'
import ForgotPassword from './pages/forgotpassword/ForgotPassword'
import PageNotFound from './pages/misc/PageNotFound'
import UnderMaintenence from './pages/misc/UnderMaintenence'
import Dashboard from './pages/backend/dashboard/Dashboard'
import Product from './pages/backend/product/Product'
const PageRoutes = () => {
    return(
        <Routes>
            {/* Front-end */}
            <Route path="/" exact={true} element={<Login />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/forgotpassword" element={<ForgotPassword />} />

            {/* Back-end */}
            <Route path="/backend/dashboard" element={<Dashboard />} />
            <Route path="/backend/product" element={<Product />} />
            <Route path="*" element={<PageNotFound />} /> 
            <Route path="/undermaintenence" element={<UnderMaintenence />} />

        </Routes>
    )
}

export default PageRoutes